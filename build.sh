printf "\033[1;31mStarting Massive Cross Compiler\033[0m\n"

v -obfuscate -o src/main.c build src
v -obfuscate -compress -o obsd-ritsu -os openbsd build src
v -obfuscate -compress -o haiku-ritsu -os haiku build src
v -obfuscate -compress -o linux-ritsu build src
v -obfuscate -compress -o netbsd-ritsu -os netbsd build src
v -obfuscate -compress -o dragonfly-ritsu -os dragonfly build src
v -obfuscate -compress -o android-ritsu -os android build src
v -obfuscate -compress -o solaris-ritsu -os solaris build src
v -obfuscate -compress -os windows -o win-ritsu build src
cc -o osx-ritsu src/main.c  -std=gnu99 -Wall -Wextra -Wno-unused-variable -Wno-unused-parameter -Wno-unused-result -Wno-unused-function -Wno-missing-braces -Wno-unused-label -Werror=implicit-function-declaration -x objective-c  -x none  
echo 'Done!'